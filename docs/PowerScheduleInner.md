# IO.Swagger.Model.PowerScheduleInner
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TurbineId** | **string** |  | 
**Timeseries** | [**List&lt;MWTimeseriesElem&gt;**](MWTimeseriesElem.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

