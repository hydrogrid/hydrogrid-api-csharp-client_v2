# IO.Swagger.Api.SecondaryApi

All URIs are relative to *https://api.hydrogrid.eu/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ApiGateRequestDischargeActual**](SecondaryApi.md#apigaterequestdischargeactual) | **GET** /plant/{plant_id}/gate/discharge-actual | Get gate discharge timeseries
[**ApiGateRequestOpeningActual**](SecondaryApi.md#apigaterequestopeningactual) | **GET** /plant/{plant_id}/gate/opening-actual | Get gate opening timeseries
[**ApiPowerRequestActiveGrossActual**](SecondaryApi.md#apipowerrequestactivegrossactual) | **GET** /plant/{plant_id}/power/active-gross-actual | Get the actual power of a plant
[**ApiReservoirRequestLevel**](SecondaryApi.md#apireservoirrequestlevel) | **GET** /plant/{plant_id}/reservoir/level | Get reservoir level timeseries

<a name="apigaterequestdischargeactual"></a>
# **ApiGateRequestDischargeActual**
> InlineResponse2006 ApiGateRequestDischargeActual (string plantId, long? startTs, long? endTs)

Get gate discharge timeseries

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiGateRequestDischargeActualExample
    {
        public void main()
        {

            var apiInstance = new SecondaryApi();
            var plantId = new string(); // string | The id of the plant
            var startTs = new long?(); // long? | Start of query
            var endTs = new long?(); // long? | End of query

            try
            {
                // Get gate discharge timeseries
                InlineResponse2006 result = apiInstance.ApiGateRequestDischargeActual(plantId, startTs, endTs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SecondaryApi.ApiGateRequestDischargeActual: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plantId** | [**string**](string.md)| The id of the plant | 
 **startTs** | [**long?**](long?.md)| Start of query | 
 **endTs** | [**long?**](long?.md)| End of query | 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apigaterequestopeningactual"></a>
# **ApiGateRequestOpeningActual**
> InlineResponse2007 ApiGateRequestOpeningActual (string plantId, GateOpeningUnit unit, long? startTs, long? endTs)

Get gate opening timeseries

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiGateRequestOpeningActualExample
    {
        public void main()
        {

            var apiInstance = new SecondaryApi();
            var plantId = new string(); // string | The id of the plant
            var unit = new GateOpeningUnit(); // GateOpeningUnit | 
            var startTs = new long?(); // long? | Start of query
            var endTs = new long?(); // long? | End of query

            try
            {
                // Get gate opening timeseries
                InlineResponse2007 result = apiInstance.ApiGateRequestOpeningActual(plantId, unit, startTs, endTs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SecondaryApi.ApiGateRequestOpeningActual: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plantId** | [**string**](string.md)| The id of the plant | 
 **unit** | [**GateOpeningUnit**](GateOpeningUnit.md)|  | 
 **startTs** | [**long?**](long?.md)| Start of query | 
 **endTs** | [**long?**](long?.md)| End of query | 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apipowerrequestactivegrossactual"></a>
# **ApiPowerRequestActiveGrossActual**
> InlineResponse2002 ApiPowerRequestActiveGrossActual (string plantId, long? startTs, long? endTs)

Get the actual power of a plant

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiPowerRequestActiveGrossActualExample
    {
        public void main()
        {

            var apiInstance = new SecondaryApi();
            var plantId = new string(); // string | The id of the plant
            var startTs = new long?(); // long? | Start of query
            var endTs = new long?(); // long? | End of query

            try
            {
                // Get the actual power of a plant
                InlineResponse2002 result = apiInstance.ApiPowerRequestActiveGrossActual(plantId, startTs, endTs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SecondaryApi.ApiPowerRequestActiveGrossActual: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plantId** | [**string**](string.md)| The id of the plant | 
 **startTs** | [**long?**](long?.md)| Start of query | 
 **endTs** | [**long?**](long?.md)| End of query | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apireservoirrequestlevel"></a>
# **ApiReservoirRequestLevel**
> InlineResponse2005 ApiReservoirRequestLevel (string plantId, long? startTs, long? endTs)

Get reservoir level timeseries

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiReservoirRequestLevelExample
    {
        public void main()
        {

            var apiInstance = new SecondaryApi();
            var plantId = new string(); // string | The id of the plant
            var startTs = new long?(); // long? | Start of query
            var endTs = new long?(); // long? | End of query

            try
            {
                // Get reservoir level timeseries
                InlineResponse2005 result = apiInstance.ApiReservoirRequestLevel(plantId, startTs, endTs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SecondaryApi.ApiReservoirRequestLevel: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plantId** | [**string**](string.md)| The id of the plant | 
 **startTs** | [**long?**](long?.md)| Start of query | 
 **endTs** | [**long?**](long?.md)| End of query | 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
