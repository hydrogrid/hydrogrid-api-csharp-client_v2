# IO.Swagger.Api.RunOfRiverCoreApi

All URIs are relative to *https://api.hydrogrid.eu/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ApiPowerSubmitActiveGrossActual**](RunOfRiverCoreApi.md#apipowersubmitactivegrossactual) | **POST** /plant/{plant_id}/power/active-gross-actual | Submit power timeseries for a whole plant

<a name="apipowersubmitactivegrossactual"></a>
# **ApiPowerSubmitActiveGrossActual**
> void ApiPowerSubmitActiveGrossActual (List<PowerScheduleInner> body, string plantId)

Submit power timeseries for a whole plant

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiPowerSubmitActiveGrossActualExample
    {
        public void main()
        {

            var apiInstance = new RunOfRiverCoreApi();
            var body = new List<PowerScheduleInner>(); // List<PowerScheduleInner> | Timeseries to submit
            var plantId = new string(); // string | The id of the plant

            try
            {
                // Submit power timeseries for a whole plant
                apiInstance.ApiPowerSubmitActiveGrossActual(body, plantId);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RunOfRiverCoreApi.ApiPowerSubmitActiveGrossActual: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;PowerScheduleInner&gt;**](PowerScheduleInner.md)| Timeseries to submit | 
 **plantId** | [**string**](string.md)| The id of the plant | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
