# IO.Swagger.Model.ReservoirLevelActualInner
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReservoirId** | **string** |  | 
**Timeseries** | [**List&lt;ReservoirLevelTimeseriesElem&gt;**](ReservoirLevelTimeseriesElem.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

