# IO.Swagger.Api.ReservoirCoreApi

All URIs are relative to *https://api.hydrogrid.eu/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ApiPowerRequestActiveGrossPlan**](ReservoirCoreApi.md#apipowerrequestactivegrossplan) | **GET** /plant/{plant_id}/power/active-gross-plan | Get the planned power for a plant
[**ApiPowerSubmitActiveGrossActual**](ReservoirCoreApi.md#apipowersubmitactivegrossactual) | **POST** /plant/{plant_id}/power/active-gross-actual | Submit power timeseries for a whole plant
[**ApiReservoirSubmitLevel**](ReservoirCoreApi.md#apireservoirsubmitlevel) | **POST** /plant/{plant_id}/reservoir/level | Submit reservoir level timeseries for a reservoir

<a name="apipowerrequestactivegrossplan"></a>
# **ApiPowerRequestActiveGrossPlan**
> InlineResponse2002 ApiPowerRequestActiveGrossPlan (string plantId, long? startTs, long? endTs, int? stepSize = null)

Get the planned power for a plant

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiPowerRequestActiveGrossPlanExample
    {
        public void main()
        {

            var apiInstance = new ReservoirCoreApi();
            var plantId = new string(); // string | The id of the plant
            var startTs = new long?(); // long? | Start of query
            var endTs = new long?(); // long? | End of query
            var stepSize = new int?(); // int? | a time interval (typically used as step-size) in milliseconds. for hourly use '3600000' for 15min use '900000' (optional) 

            try
            {
                // Get the planned power for a plant
                InlineResponse2002 result = apiInstance.ApiPowerRequestActiveGrossPlan(plantId, startTs, endTs, stepSize);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReservoirCoreApi.ApiPowerRequestActiveGrossPlan: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plantId** | [**string**](string.md)| The id of the plant | 
 **startTs** | [**long?**](long?.md)| Start of query | 
 **endTs** | [**long?**](long?.md)| End of query | 
 **stepSize** | [**int?**](int?.md)| a time interval (typically used as step-size) in milliseconds. for hourly use &#x27;3600000&#x27; for 15min use &#x27;900000&#x27; | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apipowersubmitactivegrossactual"></a>
# **ApiPowerSubmitActiveGrossActual**
> void ApiPowerSubmitActiveGrossActual (List<PowerScheduleInner> body, string plantId)

Submit power timeseries for a whole plant

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiPowerSubmitActiveGrossActualExample
    {
        public void main()
        {

            var apiInstance = new ReservoirCoreApi();
            var body = new List<PowerScheduleInner>(); // List<PowerScheduleInner> | Timeseries to submit
            var plantId = new string(); // string | The id of the plant

            try
            {
                // Submit power timeseries for a whole plant
                apiInstance.ApiPowerSubmitActiveGrossActual(body, plantId);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReservoirCoreApi.ApiPowerSubmitActiveGrossActual: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;PowerScheduleInner&gt;**](PowerScheduleInner.md)| Timeseries to submit | 
 **plantId** | [**string**](string.md)| The id of the plant | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apireservoirsubmitlevel"></a>
# **ApiReservoirSubmitLevel**
> void ApiReservoirSubmitLevel (List<ReservoirLevelActualInner> body, string plantId)

Submit reservoir level timeseries for a reservoir

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiReservoirSubmitLevelExample
    {
        public void main()
        {

            var apiInstance = new ReservoirCoreApi();
            var body = new List<ReservoirLevelActualInner>(); // List<ReservoirLevelActualInner> | Timeseries to submit
            var plantId = new string(); // string | The id of the plant

            try
            {
                // Submit reservoir level timeseries for a reservoir
                apiInstance.ApiReservoirSubmitLevel(body, plantId);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReservoirCoreApi.ApiReservoirSubmitLevel: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;ReservoirLevelActualInner&gt;**](ReservoirLevelActualInner.md)| Timeseries to submit | 
 **plantId** | [**string**](string.md)| The id of the plant | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
