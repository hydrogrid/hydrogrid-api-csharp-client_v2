# IO.Swagger.Api.GateApi

All URIs are relative to *https://api.hydrogrid.eu/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ApiGateRequestDischargeActual**](GateApi.md#apigaterequestdischargeactual) | **GET** /plant/{plant_id}/gate/discharge-actual | Get gate discharge timeseries
[**ApiGateRequestDischargePlan**](GateApi.md#apigaterequestdischargeplan) | **GET** /plant/{plant_id}/gate/discharge-plan | Get the planned discharge for all gates of a plant
[**ApiGateRequestOpeningActual**](GateApi.md#apigaterequestopeningactual) | **GET** /plant/{plant_id}/gate/opening-actual | Get gate opening timeseries
[**ApiGateRequestOpeningPlan**](GateApi.md#apigaterequestopeningplan) | **GET** /plant/{plant_id}/gate/opening-plan | Get the planned opening for all gates of a plant
[**ApiGateSubmitDischargeActual**](GateApi.md#apigatesubmitdischargeactual) | **POST** /plant/{plant_id}/gate/discharge-actual | Submit gate discharge in m³/s (!PREFERRED!)
[**ApiGateSubmitOpeningActual**](GateApi.md#apigatesubmitopeningactual) | **POST** /plant/{plant_id}/gate/opening-actual | Submit gate opening in cm

<a name="apigaterequestdischargeactual"></a>
# **ApiGateRequestDischargeActual**
> InlineResponse2006 ApiGateRequestDischargeActual (string plantId, long? startTs, long? endTs)

Get gate discharge timeseries

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiGateRequestDischargeActualExample
    {
        public void main()
        {

            var apiInstance = new GateApi();
            var plantId = new string(); // string | The id of the plant
            var startTs = new long?(); // long? | Start of query
            var endTs = new long?(); // long? | End of query

            try
            {
                // Get gate discharge timeseries
                InlineResponse2006 result = apiInstance.ApiGateRequestDischargeActual(plantId, startTs, endTs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling GateApi.ApiGateRequestDischargeActual: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plantId** | [**string**](string.md)| The id of the plant | 
 **startTs** | [**long?**](long?.md)| Start of query | 
 **endTs** | [**long?**](long?.md)| End of query | 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apigaterequestdischargeplan"></a>
# **ApiGateRequestDischargePlan**
> InlineResponse2003 ApiGateRequestDischargePlan (string plantId, long? startTs, long? endTs, int? stepSize = null)

Get the planned discharge for all gates of a plant

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiGateRequestDischargePlanExample
    {
        public void main()
        {

            var apiInstance = new GateApi();
            var plantId = new string(); // string | The id of the plant
            var startTs = new long?(); // long? | Start of query
            var endTs = new long?(); // long? | End of query
            var stepSize = new int?(); // int? | a time interval (typically used as step-size) in milliseconds. for hourly use '3600000' for 15min use '900000' (optional) 

            try
            {
                // Get the planned discharge for all gates of a plant
                InlineResponse2003 result = apiInstance.ApiGateRequestDischargePlan(plantId, startTs, endTs, stepSize);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling GateApi.ApiGateRequestDischargePlan: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plantId** | [**string**](string.md)| The id of the plant | 
 **startTs** | [**long?**](long?.md)| Start of query | 
 **endTs** | [**long?**](long?.md)| End of query | 
 **stepSize** | [**int?**](int?.md)| a time interval (typically used as step-size) in milliseconds. for hourly use &#x27;3600000&#x27; for 15min use &#x27;900000&#x27; | [optional] 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apigaterequestopeningactual"></a>
# **ApiGateRequestOpeningActual**
> InlineResponse2007 ApiGateRequestOpeningActual (string plantId, GateOpeningUnit unit, long? startTs, long? endTs)

Get gate opening timeseries

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiGateRequestOpeningActualExample
    {
        public void main()
        {

            var apiInstance = new GateApi();
            var plantId = new string(); // string | The id of the plant
            var unit = new GateOpeningUnit(); // GateOpeningUnit | 
            var startTs = new long?(); // long? | Start of query
            var endTs = new long?(); // long? | End of query

            try
            {
                // Get gate opening timeseries
                InlineResponse2007 result = apiInstance.ApiGateRequestOpeningActual(plantId, unit, startTs, endTs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling GateApi.ApiGateRequestOpeningActual: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plantId** | [**string**](string.md)| The id of the plant | 
 **unit** | [**GateOpeningUnit**](GateOpeningUnit.md)|  | 
 **startTs** | [**long?**](long?.md)| Start of query | 
 **endTs** | [**long?**](long?.md)| End of query | 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apigaterequestopeningplan"></a>
# **ApiGateRequestOpeningPlan**
> InlineResponse2004 ApiGateRequestOpeningPlan (string plantId, GateOpeningUnit unit, long? startTs, long? endTs, int? stepSize = null)

Get the planned opening for all gates of a plant

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiGateRequestOpeningPlanExample
    {
        public void main()
        {

            var apiInstance = new GateApi();
            var plantId = new string(); // string | The id of the plant
            var unit = new GateOpeningUnit(); // GateOpeningUnit | 
            var startTs = new long?(); // long? | Start of query
            var endTs = new long?(); // long? | End of query
            var stepSize = new int?(); // int? | a time interval (typically used as step-size) in milliseconds. for hourly use '3600000' for 15min use '900000' (optional) 

            try
            {
                // Get the planned opening for all gates of a plant
                InlineResponse2004 result = apiInstance.ApiGateRequestOpeningPlan(plantId, unit, startTs, endTs, stepSize);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling GateApi.ApiGateRequestOpeningPlan: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plantId** | [**string**](string.md)| The id of the plant | 
 **unit** | [**GateOpeningUnit**](GateOpeningUnit.md)|  | 
 **startTs** | [**long?**](long?.md)| Start of query | 
 **endTs** | [**long?**](long?.md)| End of query | 
 **stepSize** | [**int?**](int?.md)| a time interval (typically used as step-size) in milliseconds. for hourly use &#x27;3600000&#x27; for 15min use &#x27;900000&#x27; | [optional] 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apigatesubmitdischargeactual"></a>
# **ApiGateSubmitDischargeActual**
> void ApiGateSubmitDischargeActual (List<DischargeScheduleActualInner> body, string plantId)

Submit gate discharge in m³/s (!PREFERRED!)

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiGateSubmitDischargeActualExample
    {
        public void main()
        {

            var apiInstance = new GateApi();
            var body = new List<DischargeScheduleActualInner>(); // List<DischargeScheduleActualInner> | Timeseries to submit
            var plantId = new string(); // string | The id of the plant

            try
            {
                // Submit gate discharge in m³/s (!PREFERRED!)
                apiInstance.ApiGateSubmitDischargeActual(body, plantId);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling GateApi.ApiGateSubmitDischargeActual: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;DischargeScheduleActualInner&gt;**](DischargeScheduleActualInner.md)| Timeseries to submit | 
 **plantId** | [**string**](string.md)| The id of the plant | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apigatesubmitopeningactual"></a>
# **ApiGateSubmitOpeningActual**
> void ApiGateSubmitOpeningActual (List<OpeningScheduleActualInner> body, GateOpeningUnit unit, string plantId)

Submit gate opening in cm

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiGateSubmitOpeningActualExample
    {
        public void main()
        {

            var apiInstance = new GateApi();
            var body = new List<OpeningScheduleActualInner>(); // List<OpeningScheduleActualInner> | Timeseries to submit
            var unit = new GateOpeningUnit(); // GateOpeningUnit | 
            var plantId = new string(); // string | The id of the plant

            try
            {
                // Submit gate opening in cm
                apiInstance.ApiGateSubmitOpeningActual(body, unit, plantId);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling GateApi.ApiGateSubmitOpeningActual: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;OpeningScheduleActualInner&gt;**](OpeningScheduleActualInner.md)| Timeseries to submit | 
 **unit** | [**GateOpeningUnit**](GateOpeningUnit.md)|  | 
 **plantId** | [**string**](string.md)| The id of the plant | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
