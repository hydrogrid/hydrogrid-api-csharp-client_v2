# IO.Swagger.Api.ReservoirApi

All URIs are relative to *https://api.hydrogrid.eu/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ApiReservoirRequestLevel**](ReservoirApi.md#apireservoirrequestlevel) | **GET** /plant/{plant_id}/reservoir/level | Get reservoir level timeseries
[**ApiReservoirSubmitLevel**](ReservoirApi.md#apireservoirsubmitlevel) | **POST** /plant/{plant_id}/reservoir/level | Submit reservoir level timeseries for a reservoir

<a name="apireservoirrequestlevel"></a>
# **ApiReservoirRequestLevel**
> InlineResponse2005 ApiReservoirRequestLevel (string plantId, long? startTs, long? endTs)

Get reservoir level timeseries

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiReservoirRequestLevelExample
    {
        public void main()
        {

            var apiInstance = new ReservoirApi();
            var plantId = new string(); // string | The id of the plant
            var startTs = new long?(); // long? | Start of query
            var endTs = new long?(); // long? | End of query

            try
            {
                // Get reservoir level timeseries
                InlineResponse2005 result = apiInstance.ApiReservoirRequestLevel(plantId, startTs, endTs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReservoirApi.ApiReservoirRequestLevel: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plantId** | [**string**](string.md)| The id of the plant | 
 **startTs** | [**long?**](long?.md)| Start of query | 
 **endTs** | [**long?**](long?.md)| End of query | 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apireservoirsubmitlevel"></a>
# **ApiReservoirSubmitLevel**
> void ApiReservoirSubmitLevel (List<ReservoirLevelActualInner> body, string plantId)

Submit reservoir level timeseries for a reservoir

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiReservoirSubmitLevelExample
    {
        public void main()
        {

            var apiInstance = new ReservoirApi();
            var body = new List<ReservoirLevelActualInner>(); // List<ReservoirLevelActualInner> | Timeseries to submit
            var plantId = new string(); // string | The id of the plant

            try
            {
                // Submit reservoir level timeseries for a reservoir
                apiInstance.ApiReservoirSubmitLevel(body, plantId);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReservoirApi.ApiReservoirSubmitLevel: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;ReservoirLevelActualInner&gt;**](ReservoirLevelActualInner.md)| Timeseries to submit | 
 **plantId** | [**string**](string.md)| The id of the plant | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
