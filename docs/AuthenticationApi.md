# IO.Swagger.Api.AuthenticationApi

All URIs are relative to *https://api.hydrogrid.eu/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ApiAuthGetAccessToken**](AuthenticationApi.md#apiauthgetaccesstoken) | **GET** /auth | Get JWT
[**ApiAuthGetRefreshToken**](AuthenticationApi.md#apiauthgetrefreshtoken) | **POST** /auth/refresh | Refresh JWT

<a name="apiauthgetaccesstoken"></a>
# **ApiAuthGetAccessToken**
> InlineResponse200 ApiAuthGetAccessToken ()

Get JWT

Main Authentication endpoint. Supply Basic Authentication credentials to get a JWT. Alternatively the credentials can be passed in the request body 

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiAuthGetAccessTokenExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new AuthenticationApi();

            try
            {
                // Get JWT
                InlineResponse200 result = apiInstance.ApiAuthGetAccessToken();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AuthenticationApi.ApiAuthGetAccessToken: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apiauthgetrefreshtoken"></a>
# **ApiAuthGetRefreshToken**
> InlineResponse2001 ApiAuthGetRefreshToken ()

Refresh JWT

When your jwt expired use this to get a new one by supplying the expired token and the refresh token

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiAuthGetRefreshTokenExample
    {
        public void main()
        {

            var apiInstance = new AuthenticationApi();

            try
            {
                // Refresh JWT
                InlineResponse2001 result = apiInstance.ApiAuthGetRefreshToken();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AuthenticationApi.ApiAuthGetRefreshToken: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
