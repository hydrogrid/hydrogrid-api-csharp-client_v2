# IO.Swagger.Model.GateOpeningWithModeTimeseriesElem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Timestamp** | **long?** |  | 
**Value** | [**decimal?**](BigDecimal.md) |  | 
**Mode** | **OperationMode** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

