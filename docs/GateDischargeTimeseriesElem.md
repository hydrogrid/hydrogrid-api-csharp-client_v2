# IO.Swagger.Model.GateDischargeTimeseriesElem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Timestamp** | **long?** |  | 
**Value** | [**decimal?**](BigDecimal.md) |  | 
**Mode** | [**decimal?**](BigDecimal.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

