# IO.Swagger.Model.OpeningScheduleActualInner
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GateId** | **string** |  | 
**Timeseries** | [**List&lt;GateOpeningWithModeTimeseriesElem&gt;**](GateOpeningWithModeTimeseriesElem.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

