# IO.Swagger.Model.InlineResponse2006
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | [**DischargeScheduleActual**](DischargeScheduleActual.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

