/* 
 * HYDROGRID Insight API
 *
 * This is the [HYDROGRID Insight](https://hydrogrid.eu) API documentation and specification.  <h1>Documentation</h1> <h2>Data Exchange Format</h2> <p>The actual telemetry data of reservoirs and control units (gates and turbines) is sent to HYDRIGRID Insight's API as <strong>time series</strong>, consisting of <strong>UNIX timestamps in milli-seconds</strong>.  </p> <ul>   <li><strong>Submit:</strong> Time series of <i>actual sensor readings</i> (reservoir, turbine, gate). DO NOT send planned data.</li>   <li><strong>Fetch:</strong> Time series of <i>optimized dispatch plans</i> (turbine, gate)</li> </ul> <img src=\"https://hydrogrid.eu/wp-content/uploads/2020/11/API_timeseries.svg\" alt=\"Hydrogrid Insight API - Time Series Format\" />   <h3>Possible Variants of Submitted Time Series</h3> <p>Each submitted time series is processed by HYDROGRID Insight as one consistent time series.   <br/>   You can submit the data as one of the following time series variants:</p> <img src=\"https://hydrogrid.eu/wp-content/uploads/2020/11/API_timeseries-types.svg\" alt=\"Hydrogrid Insight API - Time Series Variants\" />  <h3>Granularity of Submitted Data</h3> <p> The submitted time series (of reservoirs, turbines, gates) must have at least the same time granularity as the market granularity your plant operates in. If available, a finer granularity can submitted to HYDROGRID Insight.  </p> <samp> Example: Your plant operates in a market with a granularity = 1h and your reservoir sensor provides 10 readings per hour. To fulfill the minimum requirements, your time series should contain one reservoir level value per hour. Ideally, this would be the reservoir level value timed closest to the end of the delivery period. <!- -Example: Your plant operates in a market with a granularity = 1h and your reservoir sensor provides 10 readings per hour. To fulfill the minimal requirements, aggregate the sensor's readings of each hour to one value and send the timeseries with a granularity of one hour. - -></samp>  <p>Further requirements: </p> <ul> <li>Submit time series in a 48h rolling window</li> <li>Submit at least one sensor value a day</li> <li>HYDROGRID Insight API rejects</li>   <ul>   <li>Data that is older than two weeks</li>   <li>Data that lies in the future</li>   <li>Values below zero (turbine production, reservoir levels, gate throughput)</li>   </ul> </ul>  <h3>Units of Submitted Data</h3> <table>   <tr>     <td width=\"100px\"><strong>Sensor</strong></td>     <td width=\"100px\"><strong>Unit</strong></td>     <td><strong>Alternative Unit</strong></td>   </tr>   <tr>     <td>Reservoir</td>     <td>masl</td>     <td></td>   </tr>   <tr>     <td>Turbine</td>     <td>MW</td>     <td></td>   </tr>   <tr>     <td>Gate</td>     <td>m³/s</td>     <td>cm, %</td>   </tr> </table>  <h3>Faulty / Missing Data within Submitted Data</h3> <p> Plant's sensor might provide faulty or missing values. For both cases, kindly ensure to handle the incorrect sensor data as follows <strong>before</strong> submitting it:  </p> <table>   <tr>     <td width=\"150px\"><strong>Issue</strong></td>     <td><strong>Handling</strong></td>   </tr>   <tr>     <td>Faulty Sensor Data</td>     <td>Submit no value instead of the faulty value.</td>   </tr>   <tr>     <td>Missing Sensor Data</td>     <td>Submit no value.</td>   </tr> </table>  Sending no values yields data gaps in your submitted time series. Keep the data outage as short as possible to mitigate the optimization imprecision. HYDROGRID Insight automatically handles data gaps up to 48h (see <a href=\"#shortgaps\">Handling Short Data Outage</a>), for data outages of more than 48h kindly refer to the <a href=\"#longgaps\">Handling Long Data Outage</a>.  <h2>Details on HYDROGRID Insight</h2> <h3>Processing the Time Series of Sensor Data</h3>  <p>Submitted time series are processed by HYDROGRID Insight depending on the sensor type.</p> <ul> <li><strong>Reservoir: </strong> A reservoir sensor value <i>x</i> for a timestamp <i>t</i> refers to the reservoir level reading <i>x</i> at timestamp <i>t</i>.</li> <li><strong>Turbine/Gate: </strong> After receiving the turbine or gate sensor value <i>x</i> for a timestamp <i>t</i>, HYDROGRID Insight processes <i>x</i> after the end of the market delivery period.</li> </ul> <img src=\"https://hydrogrid.eu/wp-content/uploads/2020/11/API_timeseries-processing.svg\" alt=\"HYDROGRID Insight API - Time Series Processing\" />  <h3 id=\"shortgaps\">Handling of Short-Term Communication Faults and Data Outages (< 48h)</h3>  <p>HYDROGRID Insight automatically handles data gaps in the submitted time series up to 48h. The handling of the data gaps is explained in the following figure.</p> <img src=\"https://hydrogrid.eu/wp-content/uploads/2020/11/API_timeseries-errors.svg\" alt=\"HYDROGRID Insight API - Data Gaps Handling\" />  <h3 id=\"longgaps\">Handling of Long-Term Communication Faults & Data Outages (> 48h)</h3> <p>In case of a long data outage (more than 48h), HYDROGRID will automatically inform you about the data outage and will ask you to re-send the missing data to immediately provide optimal optimization results. </p>   <ul>   <li> If resending is not possible: HYDROGRID Insight will ignore the data gap and re-start the optimization as soon as new actual data is submitted. The optimization will be based on actual data. </li>   <li>If re-sending is possible: HYDROGRID Insight will re-calculate the optimization results for the period of the data outage and re-start the optimization as soon as new actual data is submitted. The actual hourly optimization results will be based on past and actual data. </li>   </ul>  <h1>Developer Resources</h1>  <h2>Ready to use API Client Code</h2> <p> HYDROGRID provides ready-to-use API Client code. Simply clone or download the source code from <a href=\"https://bitbucket.org/hydrogrid/\" target=\"new\">HYDROGRID API Developer Resources</a>.  </p>  <h2>Generate API Client Code in Any Language</h2> <p> In case you require a different programming / scripting languages for API Client implementation than those provided in the <a href=\"https://bitbucket.org/hydrogrid/\" target=\"new\">HYDROGRID API Developer Ressources Repository</a> you can quickly create the source code of the API Client by performing the following steps:  </p> <ol>   <li> Download the latest <a href=\"https://api-spec.hydrogrid.eu/swagger.yaml\">HYDROGRID Insight API Specification</a> (as .yaml file)    <li> Generate the code by following the instructions on <a href=\"https://github.com/OpenAPITools\" target=\"new\">OpenAPITools</a>. <ol> 
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RestSharp;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace IO.Swagger.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
        public interface IAuthenticationApi : IApiAccessor
    {
        #region Synchronous Operations
        /// <summary>
        /// Get JWT
        /// </summary>
        /// <remarks>
        /// Main Authentication endpoint. Supply Basic Authentication credentials to get a JWT. Alternatively the credentials can be passed in the request body 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>InlineResponse200</returns>
        InlineResponse200 ApiAuthGetAccessToken ();

        /// <summary>
        /// Get JWT
        /// </summary>
        /// <remarks>
        /// Main Authentication endpoint. Supply Basic Authentication credentials to get a JWT. Alternatively the credentials can be passed in the request body 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of InlineResponse200</returns>
        ApiResponse<InlineResponse200> ApiAuthGetAccessTokenWithHttpInfo ();
        /// <summary>
        /// Refresh JWT
        /// </summary>
        /// <remarks>
        /// When your jwt expired use this to get a new one by supplying the expired token and the refresh token
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>InlineResponse2001</returns>
        InlineResponse2001 ApiAuthGetRefreshToken ();

        /// <summary>
        /// Refresh JWT
        /// </summary>
        /// <remarks>
        /// When your jwt expired use this to get a new one by supplying the expired token and the refresh token
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of InlineResponse2001</returns>
        ApiResponse<InlineResponse2001> ApiAuthGetRefreshTokenWithHttpInfo ();
        #endregion Synchronous Operations
        #region Asynchronous Operations
        /// <summary>
        /// Get JWT
        /// </summary>
        /// <remarks>
        /// Main Authentication endpoint. Supply Basic Authentication credentials to get a JWT. Alternatively the credentials can be passed in the request body 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of InlineResponse200</returns>
        System.Threading.Tasks.Task<InlineResponse200> ApiAuthGetAccessTokenAsync ();

        /// <summary>
        /// Get JWT
        /// </summary>
        /// <remarks>
        /// Main Authentication endpoint. Supply Basic Authentication credentials to get a JWT. Alternatively the credentials can be passed in the request body 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (InlineResponse200)</returns>
        System.Threading.Tasks.Task<ApiResponse<InlineResponse200>> ApiAuthGetAccessTokenAsyncWithHttpInfo ();
        /// <summary>
        /// Refresh JWT
        /// </summary>
        /// <remarks>
        /// When your jwt expired use this to get a new one by supplying the expired token and the refresh token
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of InlineResponse2001</returns>
        System.Threading.Tasks.Task<InlineResponse2001> ApiAuthGetRefreshTokenAsync ();

        /// <summary>
        /// Refresh JWT
        /// </summary>
        /// <remarks>
        /// When your jwt expired use this to get a new one by supplying the expired token and the refresh token
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (InlineResponse2001)</returns>
        System.Threading.Tasks.Task<ApiResponse<InlineResponse2001>> ApiAuthGetRefreshTokenAsyncWithHttpInfo ();
        #endregion Asynchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
        public partial class AuthenticationApi : IAuthenticationApi
    {
        private IO.Swagger.Client.ExceptionFactory _exceptionFactory = (name, response) => null;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationApi"/> class.
        /// </summary>
        /// <returns></returns>
        public AuthenticationApi(String basePath)
        {
            this.Configuration = new IO.Swagger.Client.Configuration { BasePath = basePath };

            ExceptionFactory = IO.Swagger.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationApi"/> class
        /// </summary>
        /// <returns></returns>
        public AuthenticationApi()
        {
            this.Configuration = IO.Swagger.Client.Configuration.Default;

            ExceptionFactory = IO.Swagger.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public AuthenticationApi(IO.Swagger.Client.Configuration configuration = null)
        {
            if (configuration == null) // use the default one in Configuration
                this.Configuration = IO.Swagger.Client.Configuration.Default;
            else
                this.Configuration = configuration;

            ExceptionFactory = IO.Swagger.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        public String GetBasePath()
        {
            return this.Configuration.ApiClient.RestClient.BaseUrl.ToString();
        }

        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        [Obsolete("SetBasePath is deprecated, please do 'Configuration.ApiClient = new ApiClient(\"http://new-path\")' instead.")]
        public void SetBasePath(String basePath)
        {
            // do nothing
        }

        /// <summary>
        /// Gets or sets the configuration object
        /// </summary>
        /// <value>An instance of the Configuration</value>
        public IO.Swagger.Client.Configuration Configuration {get; set;}

        /// <summary>
        /// Provides a factory method hook for the creation of exceptions.
        /// </summary>
        public IO.Swagger.Client.ExceptionFactory ExceptionFactory
        {
            get
            {
                if (_exceptionFactory != null && _exceptionFactory.GetInvocationList().Length > 1)
                {
                    throw new InvalidOperationException("Multicast delegate for ExceptionFactory is unsupported.");
                }
                return _exceptionFactory;
            }
            set { _exceptionFactory = value; }
        }

        /// <summary>
        /// Gets the default header.
        /// </summary>
        /// <returns>Dictionary of HTTP header</returns>
        [Obsolete("DefaultHeader is deprecated, please use Configuration.DefaultHeader instead.")]
        public IDictionary<String, String> DefaultHeader()
        {
            return new ReadOnlyDictionary<string, string>(this.Configuration.DefaultHeader);
        }

        /// <summary>
        /// Add default header.
        /// </summary>
        /// <param name="key">Header field name.</param>
        /// <param name="value">Header field value.</param>
        /// <returns></returns>
        [Obsolete("AddDefaultHeader is deprecated, please use Configuration.AddDefaultHeader instead.")]
        public void AddDefaultHeader(string key, string value)
        {
            this.Configuration.AddDefaultHeader(key, value);
        }

        /// <summary>
        /// Get JWT Main Authentication endpoint. Supply Basic Authentication credentials to get a JWT. Alternatively the credentials can be passed in the request body 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>InlineResponse200</returns>
        public InlineResponse200 ApiAuthGetAccessToken ()
        {
             ApiResponse<InlineResponse200> localVarResponse = ApiAuthGetAccessTokenWithHttpInfo();
             return localVarResponse.Data;
        }

        /// <summary>
        /// Get JWT Main Authentication endpoint. Supply Basic Authentication credentials to get a JWT. Alternatively the credentials can be passed in the request body 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of InlineResponse200</returns>
        public ApiResponse< InlineResponse200 > ApiAuthGetAccessTokenWithHttpInfo ()
        {

            var localVarPath = "/auth";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new List<KeyValuePair<String, String>>();
            var localVarHeaderParams = new Dictionary<String, String>(this.Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
            };
            String localVarHttpContentType = this.Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/json"
            };
            String localVarHttpHeaderAccept = this.Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            // authentication (basicAuth) required
            // http basic authentication required
            if (!String.IsNullOrEmpty(this.Configuration.Username) || !String.IsNullOrEmpty(this.Configuration.Password))
            {
                localVarHeaderParams["Authorization"] = "Basic " + ApiClient.Base64Encode(this.Configuration.Username + ":" + this.Configuration.Password);
            }

            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) this.Configuration.ApiClient.CallApi(localVarPath,
                Method.GET, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("ApiAuthGetAccessToken", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<InlineResponse200>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => string.Join(",", x.Value)),
                (InlineResponse200) this.Configuration.ApiClient.Deserialize(localVarResponse, typeof(InlineResponse200)));
        }

        /// <summary>
        /// Get JWT Main Authentication endpoint. Supply Basic Authentication credentials to get a JWT. Alternatively the credentials can be passed in the request body 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of InlineResponse200</returns>
        public async System.Threading.Tasks.Task<InlineResponse200> ApiAuthGetAccessTokenAsync ()
        {
             ApiResponse<InlineResponse200> localVarResponse = await ApiAuthGetAccessTokenAsyncWithHttpInfo();
             return localVarResponse.Data;

        }

        /// <summary>
        /// Get JWT Main Authentication endpoint. Supply Basic Authentication credentials to get a JWT. Alternatively the credentials can be passed in the request body 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (InlineResponse200)</returns>
        public async System.Threading.Tasks.Task<ApiResponse<InlineResponse200>> ApiAuthGetAccessTokenAsyncWithHttpInfo ()
        {

            var localVarPath = "/auth";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new List<KeyValuePair<String, String>>();
            var localVarHeaderParams = new Dictionary<String, String>(this.Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
            };
            String localVarHttpContentType = this.Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/json"
            };
            String localVarHttpHeaderAccept = this.Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            // authentication (basicAuth) required
            // http basic authentication required
            if (!String.IsNullOrEmpty(this.Configuration.Username) || !String.IsNullOrEmpty(this.Configuration.Password))
            {
                localVarHeaderParams["Authorization"] = "Basic " + ApiClient.Base64Encode(this.Configuration.Username + ":" + this.Configuration.Password);
            }

            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) await this.Configuration.ApiClient.CallApiAsync(localVarPath,
                Method.GET, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("ApiAuthGetAccessToken", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<InlineResponse200>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => string.Join(",", x.Value)),
                (InlineResponse200) this.Configuration.ApiClient.Deserialize(localVarResponse, typeof(InlineResponse200)));
        }

        /// <summary>
        /// Refresh JWT When your jwt expired use this to get a new one by supplying the expired token and the refresh token
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>InlineResponse2001</returns>
        public InlineResponse2001 ApiAuthGetRefreshToken ()
        {
             ApiResponse<InlineResponse2001> localVarResponse = ApiAuthGetRefreshTokenWithHttpInfo();
             return localVarResponse.Data;
        }

        /// <summary>
        /// Refresh JWT When your jwt expired use this to get a new one by supplying the expired token and the refresh token
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of InlineResponse2001</returns>
        public ApiResponse< InlineResponse2001 > ApiAuthGetRefreshTokenWithHttpInfo ()
        {

            var localVarPath = "/auth/refresh";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new List<KeyValuePair<String, String>>();
            var localVarHeaderParams = new Dictionary<String, String>(this.Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
            };
            String localVarHttpContentType = this.Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/json"
            };
            String localVarHttpHeaderAccept = this.Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            // authentication (bearerAuth) required
            // bearer required
            if (!String.IsNullOrEmpty(this.Configuration.AccessToken))
            {
                localVarHeaderParams["Authorization"] = "Bearer " + this.Configuration.AccessToken;
            }

            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) this.Configuration.ApiClient.CallApi(localVarPath,
                Method.POST, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("ApiAuthGetRefreshToken", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<InlineResponse2001>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => string.Join(",", x.Value)),
                (InlineResponse2001) this.Configuration.ApiClient.Deserialize(localVarResponse, typeof(InlineResponse2001)));
        }

        /// <summary>
        /// Refresh JWT When your jwt expired use this to get a new one by supplying the expired token and the refresh token
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of InlineResponse2001</returns>
        public async System.Threading.Tasks.Task<InlineResponse2001> ApiAuthGetRefreshTokenAsync ()
        {
             ApiResponse<InlineResponse2001> localVarResponse = await ApiAuthGetRefreshTokenAsyncWithHttpInfo();
             return localVarResponse.Data;

        }

        /// <summary>
        /// Refresh JWT When your jwt expired use this to get a new one by supplying the expired token and the refresh token
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (InlineResponse2001)</returns>
        public async System.Threading.Tasks.Task<ApiResponse<InlineResponse2001>> ApiAuthGetRefreshTokenAsyncWithHttpInfo ()
        {

            var localVarPath = "/auth/refresh";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new List<KeyValuePair<String, String>>();
            var localVarHeaderParams = new Dictionary<String, String>(this.Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
            };
            String localVarHttpContentType = this.Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/json"
            };
            String localVarHttpHeaderAccept = this.Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            // authentication (bearerAuth) required
            // bearer required
            if (!String.IsNullOrEmpty(this.Configuration.AccessToken))
            {
                localVarHeaderParams["Authorization"] = "Bearer " + this.Configuration.AccessToken;
            }

            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) await this.Configuration.ApiClient.CallApiAsync(localVarPath,
                Method.POST, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("ApiAuthGetRefreshToken", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<InlineResponse2001>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => string.Join(",", x.Value)),
                (InlineResponse2001) this.Configuration.ApiClient.Deserialize(localVarResponse, typeof(InlineResponse2001)));
        }

    }
}
