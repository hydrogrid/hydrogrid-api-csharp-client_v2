/* 
 * HYDROGRID Insight API
 *
 * This is the [HYDROGRID Insight](https://hydrogrid.eu) API documentation and specification.  <h1>Documentation</h1> <h2>Data Exchange Format</h2> <p>The actual telemetry data of reservoirs and control units (gates and turbines) is sent to HYDRIGRID Insight's API as <strong>time series</strong>, consisting of <strong>UNIX timestamps in milli-seconds</strong>.  </p> <ul>   <li><strong>Submit:</strong> Time series of <i>actual sensor readings</i> (reservoir, turbine, gate). DO NOT send planned data.</li>   <li><strong>Fetch:</strong> Time series of <i>optimized dispatch plans</i> (turbine, gate)</li> </ul> <img src=\"https://hydrogrid.eu/wp-content/uploads/2020/11/API_timeseries.svg\" alt=\"Hydrogrid Insight API - Time Series Format\" />   <h3>Possible Variants of Submitted Time Series</h3> <p>Each submitted time series is processed by HYDROGRID Insight as one consistent time series.   <br/>   You can submit the data as one of the following time series variants:</p> <img src=\"https://hydrogrid.eu/wp-content/uploads/2020/11/API_timeseries-types.svg\" alt=\"Hydrogrid Insight API - Time Series Variants\" />  <h3>Granularity of Submitted Data</h3> <p> The submitted time series (of reservoirs, turbines, gates) must have at least the same time granularity as the market granularity your plant operates in. If available, a finer granularity can submitted to HYDROGRID Insight.  </p> <samp> Example: Your plant operates in a market with a granularity = 1h and your reservoir sensor provides 10 readings per hour. To fulfill the minimum requirements, your time series should contain one reservoir level value per hour. Ideally, this would be the reservoir level value timed closest to the end of the delivery period. <!- -Example: Your plant operates in a market with a granularity = 1h and your reservoir sensor provides 10 readings per hour. To fulfill the minimal requirements, aggregate the sensor's readings of each hour to one value and send the timeseries with a granularity of one hour. - -></samp>  <p>Further requirements: </p> <ul> <li>Submit time series in a 48h rolling window</li> <li>Submit at least one sensor value a day</li> <li>HYDROGRID Insight API rejects</li>   <ul>   <li>Data that is older than two weeks</li>   <li>Data that lies in the future</li>   <li>Values below zero (turbine production, reservoir levels, gate throughput)</li>   </ul> </ul>  <h3>Units of Submitted Data</h3> <table>   <tr>     <td width=\"100px\"><strong>Sensor</strong></td>     <td width=\"100px\"><strong>Unit</strong></td>     <td><strong>Alternative Unit</strong></td>   </tr>   <tr>     <td>Reservoir</td>     <td>masl</td>     <td></td>   </tr>   <tr>     <td>Turbine</td>     <td>MW</td>     <td></td>   </tr>   <tr>     <td>Gate</td>     <td>m³/s</td>     <td>cm, %</td>   </tr> </table>  <h3>Faulty / Missing Data within Submitted Data</h3> <p> Plant's sensor might provide faulty or missing values. For both cases, kindly ensure to handle the incorrect sensor data as follows <strong>before</strong> submitting it:  </p> <table>   <tr>     <td width=\"150px\"><strong>Issue</strong></td>     <td><strong>Handling</strong></td>   </tr>   <tr>     <td>Faulty Sensor Data</td>     <td>Submit no value instead of the faulty value.</td>   </tr>   <tr>     <td>Missing Sensor Data</td>     <td>Submit no value.</td>   </tr> </table>  Sending no values yields data gaps in your submitted time series. Keep the data outage as short as possible to mitigate the optimization imprecision. HYDROGRID Insight automatically handles data gaps up to 48h (see <a href=\"#shortgaps\">Handling Short Data Outage</a>), for data outages of more than 48h kindly refer to the <a href=\"#longgaps\">Handling Long Data Outage</a>.  <h2>Details on HYDROGRID Insight</h2> <h3>Processing the Time Series of Sensor Data</h3>  <p>Submitted time series are processed by HYDROGRID Insight depending on the sensor type.</p> <ul> <li><strong>Reservoir: </strong> A reservoir sensor value <i>x</i> for a timestamp <i>t</i> refers to the reservoir level reading <i>x</i> at timestamp <i>t</i>.</li> <li><strong>Turbine/Gate: </strong> After receiving the turbine or gate sensor value <i>x</i> for a timestamp <i>t</i>, HYDROGRID Insight processes <i>x</i> after the end of the market delivery period.</li> </ul> <img src=\"https://hydrogrid.eu/wp-content/uploads/2020/11/API_timeseries-processing.svg\" alt=\"HYDROGRID Insight API - Time Series Processing\" />  <h3 id=\"shortgaps\">Handling of Short-Term Communication Faults and Data Outages (< 48h)</h3>  <p>HYDROGRID Insight automatically handles data gaps in the submitted time series up to 48h. The handling of the data gaps is explained in the following figure.</p> <img src=\"https://hydrogrid.eu/wp-content/uploads/2020/11/API_timeseries-errors.svg\" alt=\"HYDROGRID Insight API - Data Gaps Handling\" />  <h3 id=\"longgaps\">Handling of Long-Term Communication Faults & Data Outages (> 48h)</h3> <p>In case of a long data outage (more than 48h), HYDROGRID will automatically inform you about the data outage and will ask you to re-send the missing data to immediately provide optimal optimization results. </p>   <ul>   <li> If resending is not possible: HYDROGRID Insight will ignore the data gap and re-start the optimization as soon as new actual data is submitted. The optimization will be based on actual data. </li>   <li>If re-sending is possible: HYDROGRID Insight will re-calculate the optimization results for the period of the data outage and re-start the optimization as soon as new actual data is submitted. The actual hourly optimization results will be based on past and actual data. </li>   </ul>  <h1>Developer Resources</h1>  <h2>Ready to use API Client Code</h2> <p> HYDROGRID provides ready-to-use API Client code. Simply clone or download the source code from <a href=\"https://bitbucket.org/hydrogrid/\" target=\"new\">HYDROGRID API Developer Resources</a>.  </p>  <h2>Generate API Client Code in Any Language</h2> <p> In case you require a different programming / scripting languages for API Client implementation than those provided in the <a href=\"https://bitbucket.org/hydrogrid/\" target=\"new\">HYDROGRID API Developer Ressources Repository</a> you can quickly create the source code of the API Client by performing the following steps:  </p> <ol>   <li> Download the latest <a href=\"https://api-spec.hydrogrid.eu/swagger.yaml\">HYDROGRID Insight API Specification</a> (as .yaml file)    <li> Generate the code by following the instructions on <a href=\"https://github.com/OpenAPITools\" target=\"new\">OpenAPITools</a>. <ol> 
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Reflection;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace IO.Swagger.Client
{
    /// <summary>
    /// Represents a set of configuration settings
    /// </summary>
        public class Configuration : IReadableConfiguration
    {
        #region Constants

        /// <summary>
        /// Version of the package.
        /// </summary>
        /// <value>Version of the package.</value>
        public const string Version = "1.0.0";

        /// <summary>
        /// Identifier for ISO 8601 DateTime Format
        /// </summary>
        /// <remarks>See https://msdn.microsoft.com/en-us/library/az4se3k1(v=vs.110).aspx#Anchor_8 for more information.</remarks>
        // ReSharper disable once InconsistentNaming
        public const string ISO8601_DATETIME_FORMAT = "o";

        #endregion Constants

        #region Static Members

        private static readonly object GlobalConfigSync = new { };
        private static Configuration _globalConfiguration;

        /// <summary>
        /// Default creation of exceptions for a given method name and response object
        /// </summary>
        public static readonly ExceptionFactory DefaultExceptionFactory = (methodName, response) =>
        {
            var status = (int)response.StatusCode;
            if (status >= 400)
            {
                return new ApiException(status,
                    string.Format("Error calling {0}: {1}", methodName, response.Content),
                    response.Content);
            }
            if (status == 0)
            {
                return new ApiException(status,
                    string.Format("Error calling {0}: {1}", methodName, response.ErrorMessage), response.ErrorMessage);
            }
            return null;
        };

        /// <summary>
        /// Gets or sets the default Configuration.
        /// </summary>
        /// <value>Configuration.</value>
        public static Configuration Default
        {
            get { return _globalConfiguration; }
            set
            {
                lock (GlobalConfigSync)
                {
                    _globalConfiguration = value;
                }
            }
        }

        #endregion Static Members

        #region Private Members

        /// <summary>
        /// Gets or sets the API key based on the authentication name.
        /// </summary>
        /// <value>The API key.</value>
        private IDictionary<string, string> _apiKey = null;

        /// <summary>
        /// Gets or sets the prefix (e.g. Token) of the API key based on the authentication name.
        /// </summary>
        /// <value>The prefix of the API key.</value>
        private IDictionary<string, string> _apiKeyPrefix = null;

        private string _dateTimeFormat = ISO8601_DATETIME_FORMAT;
        private string _tempFolderPath = Path.GetTempPath();

        #endregion Private Members

        #region Constructors

        static Configuration()
        {
            _globalConfiguration = new GlobalConfiguration();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration" /> class
        /// </summary>
        public Configuration()
        {
            UserAgent = "Swagger-Codegen/1.0.0/csharp";
            BasePath = "https://api.hydrogrid.eu/v1";
            DefaultHeader = new ConcurrentDictionary<string, string>();
            ApiKey = new ConcurrentDictionary<string, string>();
            ApiKeyPrefix = new ConcurrentDictionary<string, string>();

            Timeout = 100000;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration" /> class
        /// </summary>
        public Configuration(
            IDictionary<string, string> defaultHeader,
            IDictionary<string, string> apiKey,
            IDictionary<string, string> apiKeyPrefix,
            string basePath = "https://api.hydrogrid.eu/v1") : this()
        {
            if (string.IsNullOrWhiteSpace(basePath))
                throw new ArgumentException("The provided basePath is invalid.", "basePath");
            if (defaultHeader == null)
                throw new ArgumentNullException("defaultHeader");
            if (apiKey == null)
                throw new ArgumentNullException("apiKey");
            if (apiKeyPrefix == null)
                throw new ArgumentNullException("apiKeyPrefix");

            BasePath = basePath;

            foreach (var keyValuePair in defaultHeader)
            {
                DefaultHeader.Add(keyValuePair);
            }

            foreach (var keyValuePair in apiKey)
            {
                ApiKey.Add(keyValuePair);
            }

            foreach (var keyValuePair in apiKeyPrefix)
            {
                ApiKeyPrefix.Add(keyValuePair);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration" /> class with different settings
        /// </summary>
        /// <param name="apiClient">Api client</param>
        /// <param name="defaultHeader">Dictionary of default HTTP header</param>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="accessToken">accessToken</param>
        /// <param name="apiKey">Dictionary of API key</param>
        /// <param name="apiKeyPrefix">Dictionary of API key prefix</param>
        /// <param name="tempFolderPath">Temp folder path</param>
        /// <param name="dateTimeFormat">DateTime format string</param>
        /// <param name="timeout">HTTP connection timeout (in milliseconds)</param>
        /// <param name="userAgent">HTTP user agent</param>
        [Obsolete("Use explicit object construction and setting of properties.", true)]
        public Configuration(
            // ReSharper disable UnusedParameter.Local
            ApiClient apiClient = null,
            IDictionary<string, string> defaultHeader = null,
            string username = null,
            string password = null,
            string accessToken = null,
            IDictionary<string, string> apiKey = null,
            IDictionary<string, string> apiKeyPrefix = null,
            string tempFolderPath = null,
            string dateTimeFormat = null,
            int timeout = 100000,
            string userAgent = "Swagger-Codegen/1.0.0/csharp"
            // ReSharper restore UnusedParameter.Local
            )
        {

        }

        /// <summary>
        /// Initializes a new instance of the Configuration class.
        /// </summary>
        /// <param name="apiClient">Api client.</param>
        [Obsolete("This constructor caused unexpected sharing of static data. It is no longer supported.", true)]
        // ReSharper disable once UnusedParameter.Local
        public Configuration(ApiClient apiClient)
        {

        }

        #endregion Constructors


        #region Properties

        private ApiClient _apiClient = null;
        /// <summary>
        /// Gets an instance of an ApiClient for this configuration
        /// </summary>
        public virtual ApiClient ApiClient
        {
            get
            {
                if (_apiClient == null) _apiClient = CreateApiClient();
                return _apiClient;
            }
        }

        private String _basePath = null;
        /// <summary>
        /// Gets or sets the base path for API access.
        /// </summary>
        public virtual string BasePath {
            get { return _basePath; }
            set {
                _basePath = value;
                // pass-through to ApiClient if it's set.
                if(_apiClient != null) {
                    _apiClient.RestClient.BaseUrl = new Uri(_basePath);
                }
            }
        }

        /// <summary>
        /// Gets or sets the default header.
        /// </summary>
        public virtual IDictionary<string, string> DefaultHeader { get; set; }

        private int _timeout = 100000;
        /// <summary>
        /// Gets or sets the HTTP timeout (milliseconds) of ApiClient. Default to 100000 milliseconds.
        /// </summary>
        public virtual int Timeout
        {
            
            get
            {
                if (_apiClient == null)
                {
                    return _timeout;
                } 
                else
                {
                    return ApiClient.RestClient.Timeout;
                }
            }
            set
            {
                _timeout = value;
                if (_apiClient != null)
                {
                    ApiClient.RestClient.Timeout = _timeout;
                }
            }
        }

        /// <summary>
        /// Gets or sets the HTTP user agent.
        /// </summary>
        /// <value>Http user agent.</value>
        public virtual string UserAgent { get; set; }

        /// <summary>
        /// Gets or sets the username (HTTP basic authentication).
        /// </summary>
        /// <value>The username.</value>
        public virtual string Username { get; set; }

        /// <summary>
        /// Gets or sets the password (HTTP basic authentication).
        /// </summary>
        /// <value>The password.</value>
        public virtual string Password { get; set; }

        /// <summary>
        /// Gets the API key with prefix.
        /// </summary>
        /// <param name="apiKeyIdentifier">API key identifier (authentication scheme).</param>
        /// <returns>API key with prefix.</returns>
        public string GetApiKeyWithPrefix(string apiKeyIdentifier)
        {
            var apiKeyValue = "";
            ApiKey.TryGetValue (apiKeyIdentifier, out apiKeyValue);
            var apiKeyPrefix = "";
            if (ApiKeyPrefix.TryGetValue (apiKeyIdentifier, out apiKeyPrefix))
                return apiKeyPrefix + " " + apiKeyValue;
            else
                return apiKeyValue;
        }

        /// <summary>
        /// Gets or sets the access token for OAuth2 authentication.
        /// </summary>
        /// <value>The access token.</value>
        public virtual string AccessToken { get; set; }

        /// <summary>
        /// Gets or sets the temporary folder path to store the files downloaded from the server.
        /// </summary>
        /// <value>Folder path.</value>
        public virtual string TempFolderPath
        {
            get { return _tempFolderPath; }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    // Possible breaking change since swagger-codegen 2.2.1, enforce a valid temporary path on set.
                    _tempFolderPath = Path.GetTempPath();
                    return;
                }

                // create the directory if it does not exist
                if (!Directory.Exists(value))
                {
                    Directory.CreateDirectory(value);
                }

                // check if the path contains directory separator at the end
                if (value[value.Length - 1] == Path.DirectorySeparatorChar)
                {
                    _tempFolderPath = value;
                }
                else
                {
                    _tempFolderPath = value + Path.DirectorySeparatorChar;
                }
            }
        }

        /// <summary>
        /// Gets or sets the the date time format used when serializing in the ApiClient
        /// By default, it's set to ISO 8601 - "o", for others see:
        /// https://msdn.microsoft.com/en-us/library/az4se3k1(v=vs.110).aspx
        /// and https://msdn.microsoft.com/en-us/library/8kb3ddd4(v=vs.110).aspx
        /// No validation is done to ensure that the string you're providing is valid
        /// </summary>
        /// <value>The DateTimeFormat string</value>
        public virtual string DateTimeFormat
        {
            get { return _dateTimeFormat; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    // Never allow a blank or null string, go back to the default
                    _dateTimeFormat = ISO8601_DATETIME_FORMAT;
                    return;
                }

                // Caution, no validation when you choose date time format other than ISO 8601
                // Take a look at the above links
                _dateTimeFormat = value;
            }
        }

        /// <summary>
        /// Gets or sets the prefix (e.g. Token) of the API key based on the authentication name.
        /// </summary>
        /// <value>The prefix of the API key.</value>
        public virtual IDictionary<string, string> ApiKeyPrefix
        {
            get { return _apiKeyPrefix; }
            set
            {
                if (value == null)
                {
                    throw new InvalidOperationException("ApiKeyPrefix collection may not be null.");
                }
                _apiKeyPrefix = value;
            }
        }

        /// <summary>
        /// Gets or sets the API key based on the authentication name.
        /// </summary>
        /// <value>The API key.</value>
        public virtual IDictionary<string, string> ApiKey
        {
            get { return _apiKey; }
            set
            {
                if (value == null)
                {
                    throw new InvalidOperationException("ApiKey collection may not be null.");
                }
                _apiKey = value;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Add default header.
        /// </summary>
        /// <param name="key">Header field name.</param>
        /// <param name="value">Header field value.</param>
        /// <returns></returns>
        public void AddDefaultHeader(string key, string value)
        {
            DefaultHeader[key] = value;
        }

        /// <summary>
        /// Creates a new <see cref="ApiClient" /> based on this <see cref="Configuration" /> instance.
        /// </summary>
        /// <returns></returns>
        public ApiClient CreateApiClient()
        {
            return new ApiClient(BasePath) { Configuration = this };
        }


        /// <summary>
        /// Returns a string with essential information for debugging.
        /// </summary>
        public static String ToDebugReport()
        {
            String report = "C# SDK (IO.Swagger) Debug Report:\n";
            report += "    OS: " + System.Environment.OSVersion + "\n";
            report += "    .NET Framework Version: " + System.Environment.Version  + "\n";
            report += "    Version of the API: 1.0.0\n";
            report += "    SDK Package Version: 1.0.0\n";

            return report;
        }

        /// <summary>
        /// Add Api Key Header.
        /// </summary>
        /// <param name="key">Api Key name.</param>
        /// <param name="value">Api Key value.</param>
        /// <returns></returns>
        public void AddApiKey(string key, string value)
        {
            ApiKey[key] = value;
        }

        /// <summary>
        /// Sets the API key prefix.
        /// </summary>
        /// <param name="key">Api Key name.</param>
        /// <param name="value">Api Key value.</param>
        public void AddApiKeyPrefix(string key, string value)
        {
            ApiKeyPrefix[key] = value;
        }

        #endregion Methods
    }
}
